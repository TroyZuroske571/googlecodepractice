package trz.google.practice;

import java.util.LinkedList;

public class BreakUpInt {
	public static void main(String a[]){
		int number = Integer.parseInt(a[0]); 
		int revNum = Integer.parseInt(a[0]); 
		LinkedList<Integer> stack = new LinkedList<Integer>();
		while (number > 0) {
		    stack.push( number % 10 );
		    number = number / 10;
		}
	
		while (!stack.isEmpty()) {
		    System.out.println((stack.pop()));
		}
		int reverse = reverseNumber(revNum); 
		System.out.println(reverse);
	}
	
	public static int reverseNumber(int number){
        
        int reverse = 0;
        while(number != 0){
            reverse = (reverse*10)+(number%10);
            number = number/10;
        }
        return reverse;
    }
}
